package ru.t1.rleonov.tm.api.service;

import ru.t1.rleonov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
