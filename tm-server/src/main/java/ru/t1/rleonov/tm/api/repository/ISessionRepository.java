package ru.t1.rleonov.tm.api.repository;

import ru.t1.rleonov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
