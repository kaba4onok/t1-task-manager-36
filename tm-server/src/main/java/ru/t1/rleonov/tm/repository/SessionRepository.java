package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.repository.ISessionRepository;
import ru.t1.rleonov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
