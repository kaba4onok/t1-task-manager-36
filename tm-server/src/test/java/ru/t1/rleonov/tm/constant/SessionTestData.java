package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.Session;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static ru.t1.rleonov.tm.constant.UserTestData.*;


public final class SessionTestData {

    @NotNull
    public final static Session SESSION1 = new Session();

    @NotNull
    public final static Session SESSION2 = new Session();

    @NotNull
    public final static Session SESSION_ADMIN = new Session();

    @NotNull
    public final static List<Session> SESSIONS = Arrays.asList(SESSION1, SESSION2, SESSION_ADMIN);

    @NotNull
    public final static List<Session> USER1_SESSIONS = Collections.singletonList(SESSION1);

    @NotNull
    public final static List<Session> USER2_SESSIONS = Collections.singletonList(SESSION2);

    static {
        SESSION1.setUserId(USER1.getId());
        SESSION2.setUserId(USER2.getId());
        SESSION_ADMIN.setUserId(ADMIN.getId());
    }

}
