package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.Task;
import java.util.ArrayList;
import java.util.List;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.USER2;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @Test
    public void findAllByProjectId() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @NotNull final String project_id = USER1_PROJECT1.getId();
        Assert.assertEquals(USER1_PROJECT1_TASKS, repository.findAllByProjectId(user_id, project_id));
    }

    @Test
    public void create() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @Nullable final String name = USER1_TASK1.getName();
        @Nullable final String description = USER1_TASK1.getDescription();
        @Nullable final Task Task = repository.create(user_id, name, description);
        Assert.assertEquals(user_id, Task.getUserId());
        Assert.assertEquals(name, Task.getName());
        Assert.assertEquals(description, Task.getDescription());
    }

    @Test
    public void createNoDesc() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @Nullable final String name = USER1_TASK1.getName();
        @Nullable final Task Task = repository.create(user_id, name);
        Assert.assertEquals(user_id, Task.getUserId());
        Assert.assertEquals(name, Task.getName());
    }

    @Test
    public void getSize() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        Assert.assertEquals(ALL_TASKS.size(), repository.getSize());
    }

    @Test
    public void add() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USER1_TASKS);
        repository.add(USER2_TASK1);
        Assert.assertEquals(USER2_TASK1, repository.findOneByIndex(2));
    }

    @Test
    public void findAll() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        Assert.assertEquals(USER2_TASKS, repository.findAll(USER2.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final List<Task> sortedTasks = new ArrayList<>(ALL_TASKS);
        sortedTasks.sort(CreatedComparator.INSTANCE);
        repository.set(sortedTasks);
        Assert.assertEquals(USER2_TASKS, repository.findAll(USER2.getId(), CreatedComparator.INSTANCE));
    }

    @Test
    public void existsById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @NotNull final String Task_id = USER1_TASK1.getId();
        Assert.assertNotNull(repository.findOneById(user_id, Task_id));
    }

    @Test
    public void findOneById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @NotNull final String Task_id = USER1_TASK1.getId();
        Assert.assertEquals(USER1_TASK1, repository.findOneById(user_id, Task_id));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @NotNull final Integer index = USER1_TASKS.indexOf(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, repository.findOneByIndex(user_id, index));
    }

    @Test
    public void remove() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        @Nullable final String user_id = USER1_TASK1.getUserId();
        repository.remove(user_id, USER1_TASK1);
        Assert.assertEquals(USER1_TASK2, repository.findOneByIndex(user_id, 0));
    }

    @Test
    public void removeById() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @NotNull final String Task_id = USER1_TASK1.getId();
        repository.removeById(user_id, Task_id);
        Assert.assertEquals(USER1_TASK2, repository.findOneByIndex(user_id, 0));
    }

    @Test
    public void removeByIndex() {
        @NotNull final ITaskRepository repository = new TaskRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_TASKS);
        @Nullable final String user_id = USER1_TASK1.getUserId();
        @NotNull final Integer index = USER1_TASKS.indexOf(USER1_TASK1);
        repository.removeByIndex(user_id, index);
        Assert.assertEquals(USER1_TASK2, repository.findOneByIndex(user_id, 0));
    }
    
}
