package ru.t1.rleonov.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
