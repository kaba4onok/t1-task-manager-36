package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.rleonov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.marker.IntegrationCategory;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.service.PropertyService;
import ru.t1.rleonov.tm.util.TestUtil;
import java.util.Collections;
import java.util.List;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String user1Token;

    @Nullable
    private static String user2Token;

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUpClass() {
        user1Token = TestUtil.login(USER1);
        user2Token = TestUtil.login(USER2);
        adminToken = TestUtil.login(ADMIN);
    }

    @AfterClass
    public static void resetClass() {
        TestUtil.logout(user1Token);
        TestUtil.logout(user2Token);
        TestUtil.logout(adminToken);
    }

    @NotNull
    private static List<Task> getUserTaskList (@NotNull String token) {
        @NotNull final TaskListRequest request = new TaskListRequest(token);
        @Nullable final List<Task> tasks = taskEndpoint.getTaskList(request).getTasks();
        if (tasks == null) return Collections.emptyList();
        return tasks;
    }

    @NotNull
    private static List<Project> getUserProjectList (@NotNull String token) {
        @NotNull final ProjectListRequest request = new ProjectListRequest(token);
        @Nullable final List<Project> projects = projectEndpoint.getProjectList(request).getProjects();
        if (projects == null) return Collections.emptyList();
        return projects;
    }

    @Before
    public void setUpMethod() {
        TestUtil.reloadData(adminToken);
    }

    @Test
    public void taskBindToProject() {
        @NotNull final String projectId = getUserProjectList(user1Token).get(1).getId();
        @NotNull final Task task = getUserTaskList(user1Token).get(0);
        @NotNull final String taskId = task.getId();
        Assert.assertNotEquals(projectId, task.getProjectId());
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(user1Token);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(request);
        Assert.assertEquals(projectId, getUserTaskList(user1Token).get(0).getProjectId());
    }

    @Test
    public void taskUnbindFromProject() {
        @NotNull final String projectId = getUserProjectList(user1Token).get(0).getId();
        @NotNull final Task task = getUserTaskList(user1Token).get(0);
        @NotNull final String taskId = task.getId();
        Assert.assertEquals(projectId, task.getProjectId());
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(user1Token);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.unbindTaskToProject(request);
        Assert.assertNull(getUserTaskList(user1Token).get(0).getProjectId());
    }

    @Test
    public void taskShowByProjectId() {
        @NotNull final String projectId = getUserProjectList(user1Token).get(0).getId();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(user1Token);
        request.setProjectId(projectId);
        @Nullable final List<Task> tasks = taskEndpoint.getTaskListByProjectId(request).getTasks();
        Assert.assertEquals(USER1_TASKS.size(), tasks.size());
        Assert.assertEquals(USER1_TASKS.get(0).getName(), tasks.get(0).getName());
        Assert.assertEquals(USER1_TASKS.get(1).getName(), tasks.get(1).getName());
    }

    @Test
    public void taskChangeStatusById () {
        @NotNull final String taskId = getUserTaskList(user1Token).get(0).getId();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(user1Token);
        request.setId(taskId);
        request.setStatus(Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.changeTaskStatusById(request)
                .getTask().getStatus());
    }

    @Test
    public void taskChangeStatusByIndex () {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(user1Token);
        request.setIndex(0);
        request.setStatus(Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskEndpoint.changeTaskStatusByIndex(request)
                .getTask().getStatus());
    }

    @Test
    public void taskClear() {
        Assert.assertNotNull(getUserTaskList(user1Token));
        @NotNull final TaskClearRequest request = new TaskClearRequest(user1Token);
        taskEndpoint.clearTask(request);
        Assert.assertEquals(Collections.emptyList(), getUserTaskList(user1Token));
    }

    @Test
    public void taskCompleteById () {
        @NotNull final String taskId = getUserTaskList(user1Token).get(1).getId();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(user1Token);
        request.setId(taskId);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.completeTaskById(request)
                .getTask().getStatus());
    }

    @Test
    public void taskCompleteByIndex () {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(user1Token);
        request.setIndex(1);
        Assert.assertEquals(Status.COMPLETED, taskEndpoint.completeTaskByIndex(request)
                .getTask().getStatus());
    }

    @Test
    public void taskCreate() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(user1Token);
        request.setName(NEW_TASK.getName());
        request.setDescription(NEW_TASK.getDescription());
        taskEndpoint.createTask(request);
        Assert.assertEquals(NEW_TASK.getName(), taskEndpoint.createTask(request).getTask().getName());
    }

    @Test
    public void getTaskList() {
        @NotNull final List<Task> tasks = getUserTaskList(user1Token);
        Assert.assertEquals(USER1_TASKS.size(), tasks.size());
        Assert.assertEquals(USER1_TASKS.get(0).getName(), tasks.get(0).getName());
        Assert.assertEquals(USER1_TASKS.get(1).getName(), tasks.get(1).getName());
    }

    @Test
    public void taskRemoveById() {
        @NotNull final List<Task> tasks = getUserTaskList(user1Token);
        final int size = USER1_TASKS.size();
        Assert.assertEquals(size, tasks.size());
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(user1Token);
        request.setId(tasks.get(0).getId());
        Assert.assertEquals(USER1_TASKS.get(0).getName(),
                taskEndpoint.removeTaskById(request).getTask().getName());
        Assert.assertEquals(size - 1, getUserTaskList(user1Token).size());
    }

    @Test
    public void taskRemoveByIndex() {
        @NotNull final List<Task> tasks = getUserTaskList(user1Token);
        final int size = USER1_TASKS.size();
        Assert.assertEquals(size, tasks.size());
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(user1Token);
        request.setIndex(0);
        Assert.assertEquals(USER1_TASKS.get(0).getName(),
                taskEndpoint.removeTaskByIndex(request).getTask().getName());
        Assert.assertEquals(size - 1, getUserTaskList(user1Token).size());
    }

    @Test
    public void taskShowById() {
        @NotNull final String taskId = getUserTaskList(user1Token).get(0).getId();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(user1Token);
        request.setId(taskId);
        Assert.assertEquals(TASK1.getName(), taskEndpoint.showTaskById(request).getTask().getName());
    }

    @Test
    public void taskShowByIndex() {
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(user1Token);
        request.setIndex(0);
        Assert.assertEquals(TASK1.getName(), taskEndpoint.showTaskByIndex(request).getTask().getName());
    }

    @Test
    public void taskStartById () {
        @NotNull final String taskId = getUserTaskList(user1Token).get(0).getId();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(user1Token);
        request.setId(taskId);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.startTaskById(request)
                .getTask().getStatus());
    }

    @Test
    public void taskStartByIndex () {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(user1Token);
        request.setIndex(0);
        Assert.assertEquals(Status.IN_PROGRESS, taskEndpoint.startTaskByIndex(request)
                .getTask().getStatus());
    }

    @Test
    public void taskUpdateById () {
        @NotNull final String taskId = getUserTaskList(user1Token).get(0).getId();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(user1Token);
        request.setId(taskId);
        request.setName(NEW_TASK.getName());
        request.setName(NEW_TASK.getDescription());
        Assert.assertEquals(NEW_TASK.getName(), taskEndpoint.updateTaskById(request)
                .getTask().getName());
    }

    @Test
    public void taskUpdateByIndex () {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(user1Token);
        request.setIndex(0);
        request.setName(NEW_TASK.getName());
        request.setName(NEW_TASK.getDescription());
        Assert.assertEquals(NEW_TASK.getName(), taskEndpoint.updateTaskByIndex(request)
                .getTask().getName());
    }

}
