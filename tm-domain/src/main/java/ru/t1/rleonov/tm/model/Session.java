package ru.t1.rleonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.model.IWBS;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.enumerated.Status;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.IN_PROGRESS;

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

    @NotNull
    @Override
    public Date getCreated() {
        return date;
    }

    @Override
    public void setCreated(@NotNull Date created) {
        this.date = created;
    }

}
